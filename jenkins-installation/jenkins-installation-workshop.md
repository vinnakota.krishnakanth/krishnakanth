<!-- Headings -->
# To Install Jenkins
## Step-1 : Prerequisites
* *Set up Ubuntu server and run the following commands for installing jenkins*
## Step-2 : Setup Jenkins Server
<!-- Blockquote -->
<!-- italics -->
* *Clone the repository from gitlab*
```
git clone https://gitlab.com/KaluvalaRamya/krishnakanth.git
```
* *change the directory and give full permissions to owner and execute the script of installation of kind-k8s-cluster with 3 nodes (1 master & 2 worker)*
```
cd krishnakanth/jenkins-installation && chmod 755 install_java8.sh && ./install_java8.sh
```
## Step-3 : Install the Jenkins Plugins
* *Install the plugins in jenkins by navigating to Manage Jenkins -> Manage Plugins -> Available*
* *Search for the plugins*
* *Click* * Install without restart
